import React from "react";
import "./App.css";
import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import image1 from "./assets/image1.png";
import image2 from "./assets/image2.png";
import image3 from "./assets/image3.png";
import step1 from "./assets/step1.png";
import step2 from "./assets/step2.png";
import step3 from "./assets/step3.png";
import step4 from "./assets/step4.png";
import explore from "./assets/explore.png";

const App = () => {
  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <div className="nav">
            <div className="logo">Mariswap</div>
          </div>
        </Grid>
        <Grid item xs={12}>
          <Grid container>
            <Grid item xs={6}>
              <div className="header-left">
                <div className="subtitle1">Fall in love again</div>
                <div className="subtitle2">with Marimekko</div>
                <div className="description">
                  <p>Swap your loved marimekko with other’s.</p>
                  <p>Create new memories while sharing yours.</p>
                  <p>All with no expense.</p>
                </div>
                <div>
                  <button className="button">Find now</button>
                </div>
              </div>
            </Grid>
            <Grid item xs={6}>
              <div className="header-right">
                <div className="card card-1">
                  <div className="card-image">
                    <img
                      src={image1}
                      style={{ maxWidth: "100%" }}
                      alt="sweater"
                    />
                  </div>
                  <div className="card-quote">
                    Gave me warmth through the coldest time.
                  </div>
                  <div className="quote-name">- Ari</div>
                </div>
                <div className="card card-2">
                  <div className="card-image image2">
                    <img
                      src={image2}
                      style={{ maxWidth: "100%" }}
                      alt="dress"
                    />
                  </div>
                  <div className="card-quote">
                    Inspired confidence to walk through my life.
                  </div>
                  <div className="quote-name">- Annie</div>
                </div>
                <div className="card card-3">
                  <div className="card-image image3">
                    <img
                      src={image3}
                      style={{ maxWidth: "100%" }}
                      alt="dress"
                    />
                  </div>
                  <div className="card-quote">
                    My local company through long nights.
                  </div>
                  <div className="quote-name">- Lena</div>
                </div>
              </div>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          style={{ display: "flex", justifyContent: "center" }}
        >
          <Grid container spacing={4} className="step-wrapper">
            <Grid item xs={12} style={{ marginTop: 64, marginBottom: 64 }}>
              <div className="subtitle1" style={{ textAlign: "center" }}>
                WHERE TO START?
              </div>
            </Grid>
            <Grid container style={{ marginTop: 64 }}>
              <Grid item xs={6}>
                <div className="step-title">Step 1.</div>
                <div className="step-subtitle">Fill in our swap form</div>
                <div className="step-desc">
                  Take a picture and share your memories with marimekko
                  community.
                </div>
                <div className="step-desc" style={{ marginTop: 16 }}>
                  Tell us what you are looking and we will help you 😊
                </div>
              </Grid>
              <Grid item xs={6}>
                <img src={step1} width="400" />
              </Grid>
            </Grid>

            <Grid container style={{ marginTop: 64 }}>
              <Grid item xs={6}>
                <img src={step2} width="400" />
              </Grid>
              <Grid item xs={6}>
                <div className="step-title">Step 2.</div>
                <div className="step-subtitle">KYC = Know Your Clothes</div>
                <div className="step-desc">
                  Bring your loved one to any marimekko store, and we will
                  verify and approve your post if everything's fine.
                </div>
                <div className="step-desc" style={{ marginTop: 16 }}>
                  Don’t worry, we will store it with ❤️{" "}
                </div>
              </Grid>
            </Grid>

            <Grid container style={{ marginTop: 64 }}>
              <Grid item xs={6}>
                <div className="step-title">Step 3.</div>
                <div className="step-subtitle">Match time!</div>
                <div className="step-desc">
                  Accept a match reccomendation or a swap bid. After both party
                  accept the swap the item will be sent.
                </div>
                <div className="step-desc" style={{ marginTop: 16 }}>
                  <strong>Pro-tips:</strong> you can decline any recommendations
                  or offers 😉{" "}
                </div>
              </Grid>
              <Grid item xs={6}>
                <img src={step3} width="400" />
              </Grid>
            </Grid>

            <Grid container style={{ marginTop: 64 }}>
              <Grid item xs={6}>
                <img src={step4} width="300" />
              </Grid>
              <Grid item xs={6}>
                <div className="step-title">Step 4.</div>
                <div className="step-subtitle">Get your love (+ mariwards)</div>
                <div className="step-desc">
                  Collect your new item from the closest marimekko branch and
                  receive our <strong>mariwards.</strong>
                </div>
                <div className="step-desc" style={{ marginTop: 16 }}>
                  <strong>Fun fact: </strong>5 mariwards = 5% off your next
                  purchase on our official marimekko store!
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid
          item
          xs={12}
          style={{ display: "flex", justifyContent: "center" }}
        >
          <Grid container spacing={4} className="step-wrapper">
            <Grid item xs={12} style={{ marginTop: 64, marginBottom: 64 }}>
              <div className="subtitle1" style={{ textAlign: "center" }}>
                EXPLORE
              </div>
              <div style={{ marginTop: 64 }}>
                <img src={explore} width="800" />
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default App;

// a new crush on Marimekko sweater from 1996?
//
