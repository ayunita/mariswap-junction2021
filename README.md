# Junction 2021 Hackathon

Challenge: [https://www.junction2021.com/challenges/marimekko](https://www.junction2021.com/challenges/marimekko)

## Mariswap

A platform to exchange pre-loved Marimekko items.

Figma: [https://www.figma.com/file/JZcl97g5WbwwBLY6yY9RpW/Juction-2021---mariswap?node-id=41%3A590](https://www.figma.com/file/JZcl97g5WbwwBLY6yY9RpW/Juction-2021---mariswap?node-id=41%3A590)

Pitching video: [https://www.youtube.com/watch?v=mQ1fXUavRp4](https://www.youtube.com/watch?v=mQ1fXUavRp4)

## Installations

You need to have `npm` installed.

Install all dependencies
```
npm install
```

Run on localhost
```
npm run start
```

## Credits

- Illustrations: https://iconscout.com/illustration-pack/indian-doodle
- Shopping bag, scarves: marimekko website
- Explore gallery: https://www.instagram.com/marimekko/?hl=en

## Authors

- Fayya Anyatasia (pitching video maker)
- Muhammad Faiz (presenter)
- Andriani Yunita (landing page and figma maker)